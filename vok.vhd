library ieee;
use ieee.std_logic_1164.all;
use work.all;


entity vok is
    port (clk: in std_logic;
        rst    : in std_logic;
        move_v : in std_logic;
        move_o : in std_logic;
        move_k : in std_logic;
        move_c : in std_logic; 

        v : out std_logic;
        o : out std_logic;
        k : out std_logic;
        c : out std_logic);
 end vok;
architecture behavioral of vok is  
    component tff is
		port(
			clk : in std_logic;
			rst : in std_logic;
            t   : in std_logic; 
            q   : out std_logic);
	end component tff;
begin
    tff_v : tff  
        port map (
            clk => clk,
            rst => rst,
            t => move_v,
            q => v
            ); 
         
    tff_o : tff  
        port map (
            clk => clk,
            rst => rst,
            t => move_o,
            q => o
            ); 
    
    tff_k : tff  
        port map (
            clk => clk,
            rst => rst,
            t => move_k,
            q => k
            ); 
    
    tff_c : tff  
        port map (
            clk => clk,
            rst => rst,
            t => move_c,
            q => c
            ); 
            
                                  
end architecture behavioral;