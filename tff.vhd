library ieee;
use ieee.std_logic_1164.all;
entity tff is
    port (  clk : in std_logic;
            rst : in std_logic;
            t   : in std_logic; 
            q   : out std_logic);
            -- qnot : out std_logic);
end tff;

architecture TFF_arch of tff is  
    signal op: std_logic := '0';                           
    begin                                              
        process(clk) is
        begin
            if (rising_edge(clk)) then
                if(rst='1') then
                    op <= '0';
                elsif(t='1') then 
                    op <= not op;
                else 
                    op <= op;
                end if;
            end if;
        end process;
    q <= not op;                               
 end TFF_arch;