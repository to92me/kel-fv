checker vok_fv (
    clk,
    rst,
    move_v,
    move_o, 
    move_k, 
    move_c,
    v,
    o,
    k,
    c
	);
	
	default disable iff rst;

    // Vuk = v 
    // Ovca = o
    // Kupus = k
    // Camac = c 
    // V, O ili K mogu da predju na drugu stranu ako je camac na istoj strani
    // Samo jedan od V, 0 ili K mogu da se prevoze camcem  
    property move_v_p;
        @(posedge clk)
        move_v |-> move_c && ~move_o && ~move_k && ((v && c) || (~v && ~c));
    endproperty
    
    property move_o_p;
        @(posedge clk)
        move_o |-> move_c && ~move_v && ~move_k && ((o && c) || (~o && ~c));
    endproperty
    
    property move_k_p;
        @(posedge clk)
        move_k |-> move_c && ~move_v && ~move_o && ((k && c) || (~k && ~c));
    endproperty
    
    // V O i K treba da predju sa jedne strane na drugu. Strane su modelovane 
    // kao 0 i 1 outputa iz modela 
    property cover_side_0_p;
        @(posedge clk)
        v && o && k;
    endproperty

    property cover_side_1_p;
        @(posedge clk)
        ~v && ~o && ~k;
    endproperty 
   
    // ako su jednom trenutku V i O na istoj strani, u istom kloku jedan od njih mora da se ukrca na 
    // camac i predje na drugu stranu 
    property alone_v_o_p;
        @(posedge clk)
        (v && o) || (~v && ~o) |-> move_v or move_o;
    endproperty

    // isto vazi i za K i O 
    property alone_k_o_p;
        @(posedge clk)
        (o && k) || (~o && ~k) |-> move_o or move_k;
    endproperty
    
 
    cover_side_0_coverage: cover property (cover_side_0_p);
    cover_side_1_coverage: cover property (cover_side_1_p);
        
    assume_move_v: assume property(move_v_p);
    assume_move_o: assume property(move_o_p);
    assume_move_k: assume property(move_k_p);
    
    assume_alone_v_k_p: assume property(alone_v_o_p);
    assume_alone_k_o_p: assume property(alone_k_o_p);
    

endchecker