clear -all
analyze -sv09 vok.sv bind.sv
analyze -vhdl tff.vhd vok.vhd
# elaborate -top and_case
elaborate -vhdl -top vok
clock clk
reset rst
prove -bg -all